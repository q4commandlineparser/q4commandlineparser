Q4COMMANDLINEPARSER_PROJECT_INCLUDEDIR = $$Q4COMMANDLINEPARSER_BUILD_ROOT/include/Q4CommandLineParser
Q4COMMANDLINEPARSER_PROJECT_INCLUDEDIR ~=s,/,$$QMAKE_DIR_SEP,

system("$$QMAKE_MKDIR $$Q4COMMANDLINEPARSER_PROJECT_INCLUDEDIR")

for(header_file, PUBLIC_HEADERS) {
   header_file ~=s,/,$$QMAKE_DIR_SEP,
   system("$$QMAKE_COPY \"$${header_file}\" \"$$Q4COMMANDLINEPARSER_PROJECT_INCLUDEDIR\"")
}

# This is a quick workaround for generating forward header with Qt4.

!equals(QMAKE_HOST.os, Windows): maybe_quote = "\'"
system("echo $${maybe_quote}$${LITERAL_HASH}include \"qcommandlineparser.h\"$${maybe_quote} > \"$$Q4COMMANDLINEPARSER_PROJECT_INCLUDEDIR/QCommandLineParser\"")
system("echo $${maybe_quote}$${LITERAL_HASH}include \"qcommandlineoption.h\"$${maybe_quote} > \"$$Q4COMMANDLINEPARSER_PROJECT_INCLUDEDIR/QCommandLineOption\"")

PUBLIC_HEADERS += \
     $$PUBLIC_HEADERS \
     \"$$Q4COMMANDLINEPARSER_PROJECT_INCLUDEDIR/QCommandLineParser\" \
     \"$$Q4COMMANDLINEPARSER_PROJECT_INCLUDEDIR/QCommandLineOption\"

target_headers.files  = $$PUBLIC_HEADERS
target_headers.path   = $$[QT_INSTALL_HEADERS]/Q4CommandLineParser
INSTALLS              += target_headers

mkspecs_features.files    = $$Q4COMMANDLINEPARSER_PROJECT_ROOT/src/commandlineparser/qt4support/commandlineparser.prf
mkspecs_features.path     = $$[QT_INSTALL_DATA]/mkspecs/features
INSTALLS                  += mkspecs_features

win32 {
   dlltarget.path = $$[QT_INSTALL_BINS]
   INSTALLS += dlltarget
}

target.path = $$[QT_INSTALL_LIBS]
INSTALLS += target

INCLUDEPATH += $$Q4COMMANDLINEPARSER_BUILD_ROOT/include $$Q4COMMANDLINEPARSER_BUILD_ROOT/include/Q4CommandLineParser
lessThan(QT_MAJOR_VERSION, 5) {
    Q4COMMANDLINEPARSER_PROJECT_QT4SUPPORT_INCLUDEDIR = $$Q4COMMANDLINEPARSER_PROJECT_ROOT/src/commandlineparser/qt4support/include
    INCLUDEPATH += \
                   $$Q4COMMANDLINEPARSER_PROJECT_QT4SUPPORT_INCLUDEDIR \
                   $$Q4COMMANDLINEPARSER_PROJECT_QT4SUPPORT_INCLUDEDIR/QtCore \
                   $$Q4COMMANDLINEPARSER_PROJECT_QT4SUPPORT_INCLUDEDIR/private
}
DEFINES += QT_BUILD_COMMANDLINEPARSER_LIB
