CONFIG += console
CONFIG -= app_bundle
QT = core
DESTDIR = ./

SOURCES += qcommandlineparser_test_helper.cpp

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += commandlineparser
} else {
    include($$Q4COMMANDLINEPARSER_PROJECT_ROOT/src/commandlineparser/qt4support/commandlineparser.prf)
}
